package featuretestcase;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import wdMethods.SeMethods;

public class Hooks extends SeMethods {
	
	@Before
	public void before(Scenario tc) {
		startResult();
		startTestModule(tc.getName(), tc.getId());
		test = startTestCase(tc.getName());
		test.assignCategory("smoke");
		test.assignAuthor("Sathish");
		startApp("chrome", "http://leaftaps.com/opentaps");	
		
		System.out.println(tc.getName());
		System.out.println(tc.getId());
	}
	
	@After
	public void after(Scenario tc) {
		closeAllBrowsers();
		endResult();
		System.out.println(tc.getStatus());
		System.out.println(tc.isFailed());
	}

}
