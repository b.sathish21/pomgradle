/*package featuretestcase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead {

	public ChromeDriver driver;
	public String b;
	
	@Given("Open The Browser")
	public void openTheBrowser() {
	   System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	   driver = new ChromeDriver();
	}

	@Given("Max the Browser")
	public void maxTheBrowser() {
		driver.manage().window().maximize();
	}

	@Given("Set The Timeout")
	public void setTheTimeout() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Given("Launch The URL")
	public void launchTheURL() {
	   driver.get("http://leaftaps.com/opentaps");
	    
	}

	@Given("Enter The UserName as (.*)")
	public void enterTheUserNameAsDemoSalesManager(String data) {
	   driver.findElementById("username").sendKeys(data);
	    
	}

	@Given("Enter The Password as (.*)")
	public void enterThePasswordAsCrmsfa(String data) {
		driver.findElementById("password").sendKeys(data);
	    
	}

	@When("Click on the Login Button")
	public void clickOnTheLoginButton() {
	   driver.findElementByClassName("decorativeSubmit").click();
	    
	}

	@When("Click On the CRMSFA Link Button")
	public void clickOnTheCRMSFALinkButton() {
	   
		driver.findElementByLinkText("CRM/SFA").click();
	}
	
	@When("Click On the Lead Tab Button")
	public void clickOnTheLeadTabButton() {
	   
		driver.findElementByLinkText("Leads").click();
	}

	@When("Click On the Create Lead Tab Button")
	public void clickOnTheCreateLeadTabButton() {
	   
		driver.findElementByLinkText("Create Lead").click();
	}

	@Given("Enter The Company Name as (.*)")
	public void enterTheCompanyNameAsHCL(String data) {
		driver.findElementById("createLeadForm_companyName").sendKeys(data);
	    
	}

	@Given("Enter The FirstName as (.*)")
	public void enterTheFirstNameAsSathish(String data) {
		driver.findElementById("createLeadForm_firstName").sendKeys(data);	
		b = driver.findElementById("createLeadForm_firstName").getText();
	    
	}

	@Given("Enter The LastName as (.*)")
	public void enterTheLastNameAsBaskaran(String data) {
		driver.findElementById("createLeadForm_lastName").sendKeys(data);
	}

	@When("Click On The CreateLead Button")
	public void clickOnTheCreateLeadButton() {
	   
		driver.findElementByClassName("smallSubmit").click();
	}

	@Then("Verify The FirstName")
	public void verifyTheFirstName() {
	   
		String a = driver.findElementById("viewLead_firstName_sp").getText();
		if (a.contains(b)) {System.out.println("Correct");}
		else System.out.println("Not Correct");
	}

	@Then("Close The Browser")
	public void closeTheBrowser() {
	   
	    driver.close();
	}
}
*/