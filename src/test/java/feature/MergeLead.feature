Feature: Steps to Merge Lead 

Scenario: Steps For Merge Lead 
	
	Given Enter The UserName as DemoSalesManager 
	And Enter The Password as crmsfa 
	When Click on the Login Button 
	
	And Click On the CRMSFA Link Button 
	
	And Click On the Lead Tab Button 
	
	And Click On the Merge Lead Tab Button 
	
	And Click On the From Lead Find Icon Button 
	And Get the First Resulting Lead ID
	And Click On the First Resulting Lead ID in the Table Button 
	And Click On the To Lead Find Icon Button 
	And Click On the Second Resulting Lead ID in the Table Button 
	When Click On The MergeLead Button 
	
	#Then Click on the Lead Tab Button Again
	And Click on the Find Lead Tab Button
	And Enter the From Lead ID
	And Click on The FindLead Button
	And Verify the Error Message