Feature: Steps to Create Lead 

#Background: 
#	Given Open The Browser 
#	And Max the Browser 
#	And Set The Timeout 
#	And Launch The URL 
	
Scenario Outline: Steps For Creating Lead 
	
	And Enter The UserName as <uNme> 
	And Enter The Password as <pwd> 
	When Click on the Login Button 
	
	When Click On the CRMSFA Link Button 
	
	When Click On the Lead Tab Button 
	
	When Click On the Create Lead Tab Button 
	
	Given Enter The Company Name as <cNme> 
	And Enter The FirstName as <fNme> 
	And Enter The LastName as <lNme> 
	When Click On The CreateLead Button 
	
	And Verify The FirstName as <fNme>
#	And Close The Browser 
	
	Examples: 
		|uNme|pwd|cNme|fNme|lNme|
		|DemoSalesManager|crmsfa|HCL|Sathish|Baskaran|
		|DemoSalesManager|crmsfa|wipro|Jeevitha|Sathish|
		|DemoSalesManager|crmsfa|IOT|Thambi|B|