package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Then;
import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{

	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "viewLead_firstName_sp") WebElement eleverify;
	@FindBy(how = How.LINK_TEXT, using = "Leads") WebElement elelead;

	@Then("Verify The FirstName as (.*)")
	public void verifyFirstName(String fNme) {		
		verifyExactText(eleverify, fNme);
		//return new LoginPage();
	}

	public LeadPage goToLeadPage() throws InterruptedException {		

		Thread.sleep(1500);
		click(elelead);
		
		return new LeadPage();
	}

}
