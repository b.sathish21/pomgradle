package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class MyLeadPage extends ProjectMethods{

	public MyLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "createLeadForm_companyName") WebElement elecmpnyName;
	@FindBy(id = "createLeadForm_firstName") WebElement eleFstName;
	@FindBy(id = "createLeadForm_lastName") WebElement eleLstName;
	@FindBy(how = How.CLASS_NAME, using = "smallSubmit") WebElement elecrtld;
	
	@Given("Enter The Company Name as (.*)")
	public MyLeadPage enterCompanyName(String cNme) {		

		type(elecmpnyName , cNme);
		
		return this;
	}
	
	@Given("Enter The FirstName as (.*)")
	public MyLeadPage enterFirstName(String fNme) {		

		type(eleFstName , fNme);
		
		return this;
	}
	
	@Given("Enter The LastName as (.*)")
	public MyLeadPage enterLastName(String lNme) {		

		type(eleLstName , lNme);
		
		return this;
	}

	@When("Click On The CreateLead Button")
	public ViewLeadPage clickCreateLead() {		

		click(elecrtld);

		return new ViewLeadPage();
	}
}
