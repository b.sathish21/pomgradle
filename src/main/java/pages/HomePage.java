package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods{

	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.LINK_TEXT, using = "CRM/SFA") WebElement elecrmsfa;
	@FindBy(how = How.CLASS_NAME, using = "decorativeSubmit")
	WebElement eleLogout;
	
	@When("Click On the CRMSFA Link Button")
	public MyHomePage clickCRMSFA() {		
		click(elecrmsfa);
		//HomePage hp = new HomePage();
		return new MyHomePage();
	}

	public LoginPage clickLogout() {		
		click(eleLogout);
		return new LoginPage();
	}
}
