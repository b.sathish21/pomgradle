package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class MergeLeadPage extends ProjectMethods{

	public static String frmld;
	
	public MergeLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "(//img[@src='/images/fieldlookup.gif'])[1]") WebElement eleFrmLd;
	@FindBy(how = How.XPATH, using = "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]") WebElement elegtfrmLd;
	@FindBy(how = How.XPATH, using = "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[2]") WebElement elegttoLd;
	@FindBy(how = How.XPATH, using = "(//img[@src='/images/fieldlookup.gif'])[2]") WebElement eleToLd;
	@FindBy(how = How.LINK_TEXT, using = "Merge") WebElement elemrgld;

	@When("Click On the From Lead Find Icon Button")
	public MergeLeadPage clickFromLeadFindIcon() {		

		clickWithNoSnap(eleFrmLd);
		switchToWindow(1);

		return this;
	}
	
	@When("Get the First Resulting Lead ID")
	public MergeLeadPage gettingFromFindLeadID() {		

		WebDriverWait wt1 = new WebDriverWait(driver, 30);
	 	wt1.until(ExpectedConditions.elementToBeClickable(elegtfrmLd));
	 	frmld = getText(elegtfrmLd);

	 	return this;
	}
	
	@When("Click On the First Resulting Lead ID in the Table Button")
	public MergeLeadPage indentifyingFindLead() {		

		WebDriverWait wt1 = new WebDriverWait(driver, 30);
	 	wt1.until(ExpectedConditions.elementToBeClickable(elegtfrmLd));
	 	click(elegtfrmLd);
		switchToWindow(0);

		return this;
	}
	
	@When("Click On the To Lead Find Icon Button")
	public MergeLeadPage clickToLeadFindIcon() {		

		clickWithNoSnap(eleToLd);
		switchToWindow(1);

		return this;
	}
	
	@When("Click On the Second Resulting Lead ID in the Table Button")
	public MergeLeadPage indentifyingToLead() {		

		WebDriverWait wt1 = new WebDriverWait(driver, 30);
	 	wt1.until(ExpectedConditions.elementToBeClickable(elegttoLd));
		click(elegttoLd);
		switchToWindow(0);

		return this;
	}
	
	@When("Click On The MergeLead Button")
	public LeadPage clickMergeLeadButton() throws InterruptedException {		

		clickWithNoSnap(elemrgld);
		Thread.sleep(1500);
		WebDriverWait wt1 = new WebDriverWait(driver, 30);
	 	wt1.until(ExpectedConditions.alertIsPresent());
		acceptAlert();
		Thread.sleep(1500);

		return new LeadPage();
	}
}
