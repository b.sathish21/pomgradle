package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class LeadPage extends ProjectMethods{

	public LeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.LINK_TEXT, using = "Create Lead") WebElement elecrtlead;
	@FindBy(how = How.LINK_TEXT, using = "Merge Leads") WebElement elemrglead;
	@FindBy(how = How.XPATH, using = "//a[text()='Find Leads']") WebElement elefndlead;
	
	@When("Click On the Create Lead Tab Button")
	public MyLeadPage clickCreatetLead() {		
		click(elecrtlead);
		//HomePage hp = new HomePage();
		return new MyLeadPage();
	}
	@When("Click On the Merge Lead Tab Button")
	public MergeLeadPage clickMergeLeadTab() {		
		click(elemrglead);
		//HomePage hp = new HomePage();
		return new MergeLeadPage();
	}
	
	@And("Click on the Find Lead Tab Button")
	public FindLeadPage clickFindLeadTab() {		
		click(elefndlead);
		//HomePage hp = new HomePage();
		return new FindLeadPage();
	}
}
