package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class FindLeadPage extends ProjectMethods{

	public FindLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID_OR_NAME, using = "id") WebElement eleLdId;
	@FindBy(how = How.XPATH, using = "//button[text()='Find Leads']") WebElement elefndldbtn;
	@FindBy(how = How.XPATH, using = "//div[text()='No records to display']") WebElement eleerr;

	@And("Enter the From Lead ID")
	public FindLeadPage enterLeadaID() {	

		type(eleLdId,MergeLeadPage.frmld);

		return this;
	}
	
	@And("Click on The FindLead Button")
	public FindLeadPage clickFindLeadButton() {	

		click(elefndldbtn);

		return this;
	}
	
	@And("Verify the Error Message")
	public FindLeadPage verifyFromLeadRecord() {	

		String err = getText(eleerr);
		if (err.contains("No records")) System.out.println("Veerified and Found No Records");
		else System.out.println("Found Records");

		return this;
	}
}
