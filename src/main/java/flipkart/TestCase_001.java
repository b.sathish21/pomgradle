package flipkart;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class TestCase_001 {
	
	//public static void main(String[] args) throws InterruptedException {
	@Test
	public void flipkart() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver dr = new ChromeDriver();
		dr.manage().window().maximize();
		dr.get("https://www.flipkart.com/");
		dr.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(1500);
		dr.getKeyboard().sendKeys(Keys.ESCAPE);
		WebElement electronics = dr.findElementByXPath("(//li[@class='Wbt_B2 _1YVU3_'])[1]");
		Actions mouse = new Actions(dr);
		mouse.moveToElement(electronics).perform();
		dr.findElementByXPath("(//a[text()='Mi'])[1]").click();
		Thread.sleep(1500);
		String title = dr.getTitle().toString();
		if(title.contains("Mi"))
			System.out.println("Mi Mobile Phone Page is Loaded");
		else
		{
			System.out.println("general page loaded");
			dr.close();
		}
	
		dr.findElementByXPath("//div[text()='Newest First']").click();
		for(int i=1; i<=24;i++) {
			Thread.sleep(500);
			String name = dr.findElementByXPath("(//div[@class='col col-7-12'])["+i+"]/div").getText();
			Thread.sleep(500);
			String price = dr.findElementByXPath("(//div[@class='_6BWGkk'])["+i+"]/div/div").getText();
			System.out.println(name+" = "+price);
		}
		String mTitle = dr.findElementByXPath("(//div[@class='col col-7-12'])[1]/div").getText();
		dr.findElementByXPath("(//div[@class='col col-7-12'])[1]/div").click();
		Set<String> wH = dr.getWindowHandles();
		List<String> l = new ArrayList<>();
		l.addAll(wH);
		for (String a : l) {System.out.println(a);}
		dr.switchTo().window(l.get(1));	
		Thread.sleep(1500);
		
		if (dr.getTitle().contains(mTitle))
		{
			String ratings = dr.findElementByXPath("//span[@class='_38sUEc']/span/span[1]").getText();
			String reviews = dr.findElementByXPath("//span[@class='_38sUEc']/span/span[3]").getText();
			String rtng = ratings.replaceAll("\\D", "");
			String revws = reviews.replaceAll("\\D", "");
			System.out.println("The Count of Rating is "+rtng+" & Review is "+revws);
		}
		else
			System.out.println("Wrong Page Loaded");
		
		dr.quit();
	}

}
