package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class CreateLead_001 extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_LoginAndLogout";
		testDescription = "LoginAndLogout";
		authors = "sarath";
		category = "smoke";
		dataSheetName = "CreateLead";
		testNodes = "Leads";
	}
	
	@Test(dataProvider = "fetchData")
	public void login(String userName,String password, 
				String cNme, String fNme, String lNme) {		
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password).
		clickLogin()
		.clickCRMSFA()
		.clickLead01()
		.clickCreatetLead()
		.enterCompanyName(cNme)
		.enterFirstName(fNme)
		.enterLastName(lNme)
		.clickCreateLead()
		.verifyFirstName(fNme);
			
	}
	
	
}
