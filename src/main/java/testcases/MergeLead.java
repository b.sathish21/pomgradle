package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class MergeLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "Merge Lead";
		testDescription = "Merging Leads";
		authors = "Sathish";
		category = "smoke";
		dataSheetName = "MergeLead";
		testNodes = "Leads";
	}
	
	@Test(dataProvider = "fetchData")
	public void login(String userName,String password) throws InterruptedException {		
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCRMSFA()
		.clickLead01()
		.clickMergeLeadTab()
		.clickFromLeadFindIcon().indentifyingFindLead()
		.clickToLeadFindIcon().indentifyingToLead()
		.clickMergeLeadButton()
		.clickFindLeadTab()
		.enterLeadaID()
		.clickFindLeadButton()
		.verifyFromLeadRecord();
			
	}
	
	
}
